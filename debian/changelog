liquidsoap (2.3.1-1) unstable; urgency=medium

  * Wrap and sort
  * New upstream version 2.3.1
  * Drop patches fixed upstream

 -- Kyle Robbertze <paddatrapper@debian.org>  Wed, 19 Feb 2025 15:19:33 +0000

liquidsoap (2.3.0-2) unstable; urgency=medium

  * Fix building with Ocaml 5.3 (Closes: #1094720)

 -- Kyle Robbertze <paddatrapper@debian.org>  Thu, 30 Jan 2025 18:14:10 +0000

liquidsoap (2.3.0-1) unstable; urgency=medium

  * New upstream version 2.3.0 (Closes: #1072432, #1077110, #1064128)
  * Bump Standards-Version to 4.7.0 (no change)
  * Re-enable lastfm support
  * Configure cache directory
  * Refresh patches

 -- Kyle Robbertze <paddatrapper@debian.org>  Sat, 11 Jan 2025 10:40:00 +0000

liquidsoap (2.2.4-1) unstable; urgency=medium

  * New upstream version 2.2.4 (Closes: #1064128)
  * Drop gstreamer and lastfm support to fix build

 -- Kyle Robbertze <paddatrapper@debian.org>  Thu, 21 Mar 2024 16:02:22 +0000

liquidsoap (2.2.3-1) unstable; urgency=medium

  * New upstream version 2.2.3

 -- Kyle Robbertze <paddatrapper@debian.org>  Thu, 18 Jan 2024 13:51:05 +0000

liquidsoap (2.2.2-2) unstable; urgency=medium

  * Add missing Breaks+Replaces (Closes: #1057058)

 -- Kyle Robbertze <paddatrapper@debian.org>  Tue, 19 Dec 2023 10:48:42 +0000

liquidsoap (2.2.2-1) unstable; urgency=medium

  [ Stéphane Glondu ]
  * Add Rules-Requires-Root: no
  * Bump Standards-Version to 4.6.2

  [ Kyle Robbertze ]
  * Update d/watch to use new upstream artifact naming
  * Move to Dune as the build system
  * Remove old patches now upstreamed or irrelevant
  * New upstream version 2.2.2 (Closes: #1042099)
  * Drop SDL dependency (Closes: #1038078)
  * Add DSSI support
  * Split out documentation into a new package
  * Update d/copyright

 -- Kyle Robbertze <paddatrapper@debian.org>  Tue, 28 Nov 2023 09:44:39 +0000

liquidsoap (2.1.3-2) unstable; urgency=medium

  * Refresh patches
  * Prevent large memory spikes loading remote content (Closes: #1033068)

 -- Kyle Robbertze <paddatrapper@debian.org>  Tue, 28 Mar 2023 09:12:02 +0100

liquidsoap (2.1.3-1) unstable; urgency=medium

  * New upstream version 2.1.3
  * Drop patches fixed upstream
  * Build-dep on libcurl-ocaml-dev and liburi-ocaml-dev
  * Remove examples dropped upstream
  * Replace suggests youtube-dl with yt-dlp (Closes: #1024228)
  * Add new copyright information
  * Update language.dtd
  * Override source-is-missing lintian error
  * Update patches
  * Fix autopkgtests

 -- Kyle Robbertze <paddatrapper@debian.org>  Mon, 19 Dec 2022 10:15:34 +0200

liquidsoap (2.0.6-1) unstable; urgency=medium

  * Update d/watch to not track pre-releases
  * New upstream version 2.0.6

 -- Kyle Robbertze <paddatrapper@debian.org>  Tue, 12 Jul 2022 20:28:17 +0200

liquidsoap (2.0.5-1) unstable; urgency=medium

  * Bump standards-version to 4.6.1 (no change)
  * New upstream version 2.0.5

 -- Kyle Robbertze <paddatrapper@debian.org>  Fri, 27 May 2022 17:18:40 +0200

liquidsoap (2.0.2-1) unstable; urgency=medium

  * New upstream version 2.0.2 (Closes: #1002677)
  * Refresh d/patches
  * Update d/control with new build-dep versions
  * Update d/liquidsoap.links for new version
  * Update autopkgtests for new language features

 -- Kyle Robbertze <paddatrapper@debian.org>  Tue, 11 Jan 2022 11:00:10 +0200

liquidsoap (1.4.4-1) unstable; urgency=low

  * Update watchfile
  * New upstream version 1.4.4
  * Update pervasive library links
  * Upstream moved homepage location
  * Drop Breaks/Replaces for Bookworm

 -- Kyle Robbertze <paddatrapper@debian.org>  Sun, 15 Aug 2021 08:18:42 +0200

liquidsoap (1.4.3-3) unstable; urgency=medium

  * Fix pervasive libraries symlink (Closes: 991332)

 -- Kyle Robbertze <paddatrapper@debian.org>  Wed, 21 Jul 2021 16:18:07 +0200

liquidsoap (1.4.3-2) unstable; urgency=medium

  * Set BYTE install correctly (Closes: #972272)
  * Fix FTBFS type error (Closes: #976355)
  * Bump standards-version to 4.5.1 (no change)

 -- Kyle Robbertze <paddatrapper@debian.org>  Tue, 22 Dec 2020 09:17:12 +0200

liquidsoap (1.4.3-1) unstable; urgency=medium

  * Update watch file
  * New upstream version 1.4.3
  * Fix compilation with ocaml-alsa 0.3.0
  * Bump dh-compat to 13

 -- Kyle Robbertze <paddatrapper@debian.org>  Mon, 05 Oct 2020 17:46:58 +0200

liquidsoap (1.4.2-1) unstable; urgency=low

  * Link pervasive libraries into expected directory
    (Closes: #956564, LP: #1875414)
  * New upstream version 1.4.2

 -- Kyle Robbertze <paddatrapper@debian.org>  Fri, 22 May 2020 12:12:19 +0200

liquidsoap (1.4.1-1) unstable; urgency=medium

  * Install bash completions into packaging dir (Closes: #945246)
  * New upstream version 1.4.1
  * Bump Standards-Version to 4.5.0 (No change)

 -- Kyle Robbertze <paddatrapper@debian.org>  Wed, 22 Jan 2020 14:04:09 +0200

liquidsoap (1.4.0-1) unstable; urgency=medium

  * Ignore bundled releases in watch file
  * New upstream version 1.4.0 (Closes: #939580)
  * Update dependencies
  * Refresh patches
  * Use local copy of language.dtd file
  * Add patch to work around missing upstream files
  * Update install files for new release
  * Remove rpath from binary
  * Bump Standards-Version to 4.4.1 (no change)

 -- Kyle Robbertze <paddatrapper@debian.org>  Mon, 11 Nov 2019 09:27:17 +0200

liquidsoap (1.3.7-1) unstable; urgency=medium

  [ Kyle Robbertze ]
  * Drop unsatisfiable recommends on mp3gain
  * New upstream version 1.3.7
  * Install changelog
  * Update dependencies (Closes: #909448)
  * Move to debhelper-compat
  * Bump Standards-Version to 4.4.0 (No change)
  * Patch out syntax error
  * Declare recommends on emacs for elpa package
  * Refresh patches
  * Install example scripts

  [ Stephane Glondu ]
  * Remove Samuel and Romain from Uploaders

  [ Kyle Robbertze ]
  * Add override for ocaml-ssl
  * Install extract-replaygain correctly
  * Drop dh-exec useless build-dependency
  * Update copyright years
  * Drop DSSI support

 -- Kyle Robbertze <paddatrapper@debian.org>  Mon, 19 Aug 2019 22:42:52 +0200

liquidsoap (1.3.4-1) unstable; urgency=medium

  * Remove /usr/share/liquidsoap on purge (Closes: #668751)
  * New upstream version 1.3.4
  * Bump camomile version
  * Bump Standards-Version (no change)

 -- Kyle Robbertze <krobbertze@gmail.com>  Sun, 17 Feb 2019 17:35:44 +0200

liquidsoap (1.3.3-2) unstable; urgency=medium

  * Fix build errors on armel and mips*
  * Bump Standards-Version (no change)
  * Include Breaks/Replaces for old plugin packages

 -- Kyle Robbertze <krobbertze@gmail.com>  Tue, 04 Sep 2018 20:35:40 +0200

liquidsoap (1.3.3-1) unstable; urgency=medium

  * New upstream release (closes: #878896, #841871, LP: #378151)
  * Remove outdated note about MP3 encoding (closes: #721639)
  * Make copyright machine readable
  * Drop unmaintained liguidsoap binary package
  * Drop unsupported dynamic plugins for static ones
  * Bump compat to 10 (no change)
  * Bump standards version to 4.2.0 (fix user management)
  * Add user to audio group (closes: #827218)
  * Convert to use dh
  * Add new uploader

 -- Kyle Robbertze <krobbertze@gmail.com>  Mon, 06 Aug 2018 15:29:41 +0200

liquidsoap (1.1.1-7) unstable; urgency=medium

  * Patch shine encoder to adapt to new ABI.

 -- Romain Beauxis <toots@rastageeks.org>  Mon, 11 Aug 2014 20:07:58 -0500

liquidsoap (1.1.1-6) unstable; urgency=low

  * Bring back SDL plugin.

 -- Romain Beauxis <toots@rastageeks.org>  Tue, 11 Jun 2013 18:17:55 -0500

liquidsoap (1.1.1-5) unstable; urgency=low

  * Switch GD build-dep to libgd-ocaml-dev.
  * Provide alternative build-dep on libgd-gd2-ocaml-dev.
  * Bump build-dep on camlimages.
  * Drop optional SDL plugin until SDL and gd become compatible
    again (conflicts on libtiff{4,5}-dev).

 -- Romain Beauxis <toots@rastageeks.org>  Wed, 15 May 2013 07:06:38 -0500

liquidsoap (1.1.1-4) unstable; urgency=low

  * Added patch to enable dynamically loaded
    fdkaac encoder.
  * Dropped liquidsoap-plugin-fdkaac.
  Closes: #708602

 -- Romain Beauxis <toots@rastageeks.org>  Tue, 14 May 2013 17:19:49 -0500

liquidsoap (1.1.1-3) unstable; urgency=low

  * Added liquidsoap emacs mode package.
  * Merge changes from NMU 1.0.1+repack1-1.1
  * Added liquidsoap-plugin-fdkaac

 -- Romain Beauxis <toots@rastageeks.org>  Tue, 14 May 2013 00:35:34 -0500

liquidsoap (1.1.1-2) unstable; urgency=low

  * Set inotify build-dep to linux-only.

 -- Romain Beauxis <toots@rastageeks.org>  Wed, 27 Mar 2013 13:37:29 -0500

liquidsoap (1.1.1-1) unstable; urgency=low

  * New upstream release.
  * Upload to unstable.

 -- Romain Beauxis <toots@rastageeks.org>  Wed, 08 May 2013 19:48:12 -0500

liquidsoap (1.1.0-1) experimental; urgency=low

  * New upstream release.
  * Upload to experimental.
  * Enabled new plugins:
    - shine
    - frei0r
    - opus
  * Bumped standards version to 3.9.4

 -- Romain Beauxis <toots@rastageeks.org>  Sat, 20 Apr 2013 11:37:55 -0500

liquidsoap (1.0.1+repack1-1.1) unstable; urgency=low

  * Non-maintainer upload.
  * Fix "missing versioned depend on libcamomile-ocaml-data":
    make (build) dependency on libcamomile-ocaml-{dev,data} versioned
    [(>= 0.8)]. Thanks, Paul van Tilburg.
    (Closes: #685632)

 -- gregor herrmann <gregoa@debian.org>  Sat, 20 Oct 2012 19:21:43 +0200

liquidsoap (1.0.1+repack1-1) unstable; urgency=low

  * Repacked upstream tarball to remove unecessary
    .bak files
  * Bumped standard version to 3.9.3

 -- Romain Beauxis <toots@rastageeks.org>  Wed, 04 Jul 2012 19:09:46 +0200

liquidsoap (1.0.1-1) unstable; urgency=low

  * New upstream version.
  * Fixed liquidsoap.install.in for armel.
  * Dropped fix_lo_deps.patch: applied upstream.

 -- Romain Beauxis <toots@rastageeks.org>  Wed, 04 Jul 2012 00:41:16 +0200

liquidsoap (1.0.0-4) unstable; urgency=low

  * Removed wrong depedency on liquidsoap-plugin-ogg from
    liquidsoap-plugin-oss.

 -- Romain Beauxis <toots@rastageeks.org>  Fri, 21 Oct 2011 21:32:51 +0200

liquidsoap (1.0.0-3) unstable; urgency=low

  * Better patch to compute lo dymanic plugin dependencies.

 -- Romain Beauxis <toots@rastageeks.org>  Tue, 18 Oct 2011 16:43:01 +0200

liquidsoap (1.0.0-2) unstable; urgency=low

  * Fixed gstreamer, lo and camlimages plugins:
   - Build-dep on ocaml-gstreamer >= 0.1.3 (fixed
     linking options order).
   - Build-dep on camlimages >= 4.0.1-4 (fixed Sort
     weirdness).
   - Patch Makefile.rules to compute correct dependencies
     for dynamically linked modules.

 -- Romain Beauxis <toots@rastageeks.org>  Mon, 17 Oct 2011 00:17:59 +0200

liquidsoap (1.0.0-1) unstable; urgency=low

  * New upstream release.
  * Added new gstreamer plugins.
  * Removed debian/README.Debian-source: problematic files
    have been removed upstream.
  * Using ttf-liberation for default font for video.add_text
    operators.

 -- Romain Beauxis <toots@rastageeks.org>  Sun, 09 Oct 2011 16:38:00 +0200

liquidsoap (1.0.0~beta3+repack1-1) unstable; urgency=low

  * New upstream release.
  * Added new plugins:
    - lame
    - dssi
    - lo
    - camlimages
  * Added a meta-package liquidsoap-plugin-all
    which install all plugin packages.
  * Fixed FTBFS with new ocamlnet. Thanks to
    Stéphane Glondu for reporting.
  * Changed debian/rules to dynamically generate
    liquidsoap-plugin-*.onodefined files.
  Closes: #635326

 -- Romain Beauxis <toots@rastageeks.org>  Wed, 10 Aug 2011 22:36:09 -0500

liquidsoap (1.0.0~beta2.1-3) unstable; urgency=low

  * Fixed build on non-native archs.

 -- Romain Beauxis <toots@rastageeks.org>  Thu, 07 Jul 2011 14:05:07 -0500

liquidsoap (1.0.0~beta2.1-2) unstable; urgency=low

  * Really change distribution to unstable...

 -- Romain Beauxis <toots@rastageeks.org>  Thu, 07 Jul 2011 11:53:33 -0500

liquidsoap (1.0.0~beta2.1-1) experimental; urgency=low

  * New upstream release.
  * Initial upload to sid with new plugins structure.
  * Added dependencies to liquidsoap for each plugin.
  * Added liquidsoap-plugin-pulseaudio to Recommended
    packages.

 -- Romain Beauxis <toots@rastageeks.org>  Tue, 05 Jul 2011 15:01:19 -0500

liquidsoap (1.0.0~beta1+20110527~hgac5e0ed299ff-3) experimental; urgency=low

  * Fixed bytecode compilation for real: needed to pass --disable-custom
    to configure script.

 -- Romain Beauxis <toots@rastageeks.org>  Fri, 24 Jun 2011 14:20:21 -0500

liquidsoap (1.0.0~beta1+20110527~hgac5e0ed299ff-2) experimental; urgency=low

  * Do not try to install dllliquidsoap.so on non-native archs: it
    seems that with the recent patch on ocamlc, this is not needed
    anymore..

 -- Romain Beauxis <toots@rastageeks.org>  Fri, 24 Jun 2011 11:08:28 -0500

liquidsoap (1.0.0~beta1+20110527~hgac5e0ed299ff-1) experimental; urgency=low

  * New snapshot from latest mercurial code.
  * Fix compilation in bytecode.
  * Disable liquidsoap-plugin-alsa for BSD architectures.
  * Compile in bytecode on armel since dynlink.cmxa is not
    available.
  * Fixed automatic dependency computation for plugins
    using dh-ocaml >= 1.0.1

 -- Romain Beauxis <toots@rastageeks.org>  Wed, 22 Jun 2011 22:00:22 -0500

liquidsoap (1.0.0~beta1+20110525~hgbcdbe01d2de5-1) experimental; urgency=low

  * Snapshot from mercurial repository.
  * Split optional functionalities in plugins.
  * Removed bytecode compilation patch: applied upstream.

 -- Romain Beauxis <toots@rastageeks.org>  Mon, 23 May 2011 20:04:51 -0500

liquidsoap (1.0.0~beta1-2) unstable; urgency=low

  * Fixed bytecode compilation in non-custom mode.

 -- Romain Beauxis <toots@rastageeks.org>  Thu, 19 May 2011 15:47:18 -0500

liquidsoap (1.0.0~beta1-1) unstable; urgency=low

  [ Samuel Mimram ]
  * New upstream release.

  [ Romain Beauxis ]
  * Bumped standard version to 3.9.3

 -- Romain Beauxis <toots@rastageeks.org>  Tue, 17 May 2011 11:18:21 -0500

liquidsoap (0.9.3-3) unstable; urgency=low

  * Upload to unstable.
  * Enabled faad support now that is is in unstable!
  * Droped patch to add -lstdc++: should be fixed in each
    corresponding binding.
  * Added patch to support new ocaml-faad API.

 -- Romain Beauxis <toots@rastageeks.org>  Thu, 14 Apr 2011 13:56:17 -0500

liquidsoap (0.9.3-2) experimental; urgency=low

  * Added patch to enable build in non-custom mode.

 -- Romain Beauxis <toots@rastageeks.org>  Tue, 28 Sep 2010 13:46:47 -0500

liquidsoap (0.9.3-1) experimental; urgency=low

  * New upstream release.
  * Droped quilt patches: no patches needed!
  * Upload to experimental now that sid is frozen.
  * Bumped standards version to 3.9.1
  * Bumped build-dep version to match latest bindings'
    releases.
  * Changed source format to 3.0 (quilt).

 -- Romain Beauxis <toots@rastageeks.org>  Sat, 04 Sep 2010 14:59:12 -0500

liquidsoap (0.9.2-3) unstable; urgency=low

  * Fixed ogg theora default parameters.
  Closes: #577076
  * Fixed input.lastfm

 -- Romain Beauxis <toots@rastageeks.org>  Mon, 12 Apr 2010 04:06:59 +0200

liquidsoap (0.9.2-2) unstable; urgency=low

  * Patched liquidsoap to build against ocaml-theora 0.2.0~svn6618
  Closes: #574628
  * Fixed SDL output code, added libsdl-ocaml-dev in build-dep to
    enable SDL output.
  Closes: #574625
  * Added a patch to allow Magic file detection to follow symlinks.

 -- Romain Beauxis <toots@rastageeks.org>  Thu, 25 Mar 2010 11:45:29 -0500

liquidsoap (0.9.2-1) unstable; urgency=low

  * New upstream release.
  * Build against the latest ocaml-lastfm API.
  Closes: #551245
  * Ship upstream's version of the init script
    with correct dependencies.
  Closes: #541635
  * Bumped standards version to 3.8.3
  * Versioned ocaml build dependencies.
  * Add ocaml.mk support with new automatic dependency
    tracking.
  * Added patch to drop portaudio in in() definition
  * Added patch to switch mp3gain and vorbisgain to
    quiet mode.

 -- Romain Beauxis <toots@rastageeks.org>  Fri, 30 Oct 2009 12:18:05 -0500

liquidsoap (0.9.1-1) unstable; urgency=low

  * New Upstream Version
  * Dropped patch applied upstream.
  * Thightened build-deps on ocaml-duppy and
    ocaml-taglib to build against the fixed packages.
  * Updated standards version to 3.8.2

 -- Romain Beauxis <toots@rastageeks.org>  Mon, 22 Jun 2009 18:42:50 +0200

liquidsoap (0.9.0-4) unstable; urgency=low

  * Added patch from upstream:
    o Fix task end for queued sources.
    o Fix append operator.

 -- Romain Beauxis <toots@rastageeks.org>  Thu, 11 Jun 2009 02:18:50 +0200

liquidsoap (0.9.0-3) unstable; urgency=low

  * Fixed math.h usage in src/rgb_c.c
  * Rebuild against fixed modules.
  Closes: #528857

 -- Romain Beauxis <toots@rastageeks.org>  Tue, 19 May 2009 22:40:22 +1100

liquidsoap (0.9.0-2) unstable; urgency=low

  * Enabled new bindings.
  * Added suggests on mplayer.
  * Fixed clean target.

 -- Romain Beauxis <toots@rastageeks.org>  Sun, 10 May 2009 04:38:38 +0200

liquidsoap (0.9.0-1) unstable; urgency=low

  [ Stephane Glondu ]
  * Switch packaging to git.

  [ Samuel Mimram ]
  * Build with OCaml 3.11.
  * Update compat to 7.

  [ Romain Beauxis ]
  * New Upstream Version.
  * Build and upload against current
    modules. Will re-upload when new modules
    are accepted.
  * Dropped patch applied upstream.
  * Create /var/run/liquidsoap at runtime.

 -- Romain Beauxis <toots@rastageeks.org>  Thu, 02 Apr 2009 11:48:34 +0200

liquidsoap (0.3.8.1+2-2) unstable; urgency=high

  * Fix liguidsoap temporary file name.
    Thanks to Tobias Klauser for providing a patch.
  Closes: #496360
  * Desactivated portaudio option since it is buggy
    and should be more tested.

 -- Romain Beauxis <toots@rastageeks.org>  Mon, 11 Aug 2008 14:36:31 +0200

liquidsoap (0.3.8.1+2-1) unstable; urgency=low

  * There was a mistake with previous source tarball.
    Reuploading a fixed one

 -- Romain Beauxis <toots@rastageeks.org>  Mon, 11 Aug 2008 14:30:45 +0200

liquidsoap (0.3.8.1-1) unstable; urgency=low

  * New upstream release fixing several issues
    in smart_crossfade

 -- Romain Beauxis <toots@rastageeks.org>  Sat, 09 Aug 2008 17:52:53 +0200

liquidsoap (0.3.8-1) unstable; urgency=low

  * New upstream release
  * Dropped patch applied upstream
  * Versioned build-dep on ocaml-duppy to build against the fixed
    version
  Closes: #489702

 -- Romain Beauxis <toots@rastageeks.org>  Wed, 30 Jul 2008 09:46:23 +0200

liquidsoap (0.3.7-3) unstable; urgency=low

  * Rebuild against fixed ocaml-bjack
  * Bumped build-dep version for ocaml-bjack
  * Dropped patch for task leak in request_source.ml
  * Updated patch for array length in playlist.next

 -- Romain Beauxis <toots@rastageeks.org>  Mon, 23 Jun 2008 12:01:27 +0200

liquidsoap (0.3.7-2) unstable; urgency=low

  * Created /usr/share/liquidsoap directory, needed for
    liguidsoap.
  * Install logrotate file
  * Backported several fixes from upstream:
    + Fix vorbis mono output
    + Fix parameter order in documentation
    + Fix add_timeout delay propagation
    + Fix inter-thread mutex lock in playlist.ml
    + Fix task leak in request_source.ml
    + Fix array length in playlist.next
  * Enabled ocaml-bjack support

 -- Romain Beauxis <toots@rastageeks.org>  Thu, 12 Jun 2008 13:47:16 +0200

liquidsoap (0.3.7-1) unstable; urgency=low

  * New upstream release
  * Switched maintainer to the debian ocaml team
  * Dropped patches applied upstream
  * Removed ocaml-alsa build-dep for non-linux archs

 -- Romain Beauxis <toots@rastageeks.org>  Wed, 28 May 2008 21:25:41 -0400

liquidsoap (0.3.6-4) unstable; urgency=high

  * Updated fix for smartcross
  * Still setting urgency to high since testing package is
    still broken..

 -- Romain Beauxis <toots@rastageeks.org>  Tue, 04 Mar 2008 03:42:50 +0100

liquidsoap (0.3.6-3) unstable; urgency=high

  * Backported patch to fix smart_crossfading
  * Setting urgency to high since previous upload
    didn't make it to testing..

 -- Romain Beauxis <toots@rastageeks.org>  Wed, 06 Feb 2008 17:27:02 +0100

liquidsoap (0.3.6-2) unstable; urgency=high

  [ Stefano Zacchiroli ]
  * fix vcs-svn field to point just above the debian/ dir

  [ Romain Beauxis ]
  * Fixed ladspa unsafe opening: backported patch from svn.
    Closes: #464378

 -- Romain Beauxis <toots@rastageeks.org>  Wed, 06 Feb 2008 17:19:16 +0100

liquidsoap (0.3.6-1) unstable; urgency=low

  * New upstream release
  * Removed liquidtts manual page, now installed as a private script
  * Update standards to 3.7.3 (no changes)

 -- Romain Beauxis <toots@rastageeks.org>  Fri, 21 Dec 2007 02:11:03 +0100

liquidsoap (0.3.5-2) unstable; urgency=low

  * Initial upload to sid
  * Enabled all available bindings
  * Backported patch to add timeout to http connections
  * Added README.Debian with informations about mp3 output.
    (Closes: #436894)

 -- Romain Beauxis <toots@rastageeks.org>  Thu, 22 Nov 2007 11:17:08 +0100

liquidsoap (0.3.5-1) experimental; urgency=low

  * New upstream release.
  * Upload to experimental to wait for ocaml packages.

 -- Samuel Mimram <smimram@debian.org>  Mon, 12 Nov 2007 23:38:07 +0000

liquidsoap (0.3.3-3) unstable; urgency=low

  * Added liguidsoap.dpatch in order for liguidsoap not to crash on startup.
  * Removed no more needed linda-override.
  * Added missing utils.liq

 -- Samuel Mimram <smimram@debian.org>  Wed, 27 Jun 2007 00:49:22 +0200

liquidsoap (0.3.3-2) unstable; urgency=low

  * Added no-ocamlopt.dpatch in order to correcly build on non-native archs,
    closes: #430509.
  * Enable alsa support.

 -- Samuel Mimram <smimram@debian.org>  Mon, 25 Jun 2007 23:53:10 +0200

liquidsoap (0.3.3-1) unstable; urgency=low

  * New upstream release.
  * Using dpatch to handle patches.
  * Added ocamldoc.dpatch in order to correctly pass flags to ocamldoc.

 -- Samuel Mimram <smimram@debian.org>  Fri, 22 Jun 2007 22:56:49 +0200

liquidsoap (0.3.2-4) unstable; urgency=low

  * Add a missing dependency on libcamomile-ocaml-data, closes: #428096.

 -- Samuel Mimram <smimram@debian.org>  Sat, 09 Jun 2007 13:30:29 +0200

liquidsoap (0.3.2-3) unstable; urgency=low

  * Enable ocaml-ao support.
  * Explicitely disable at configure time the modules we don't use.
  * Rephrased description.
  * Added a watch file.

 -- Samuel Mimram <smimram@debian.org>  Sun, 29 Apr 2007 20:44:25 +0200

liquidsoap (0.3.2-2) unstable; urgency=low

  * Build-depend on texlive instead of tetex-extra.

 -- Samuel Mimram <smimram@debian.org>  Fri, 13 Apr 2007 18:47:27 +0200

liquidsoap (0.3.2-1) experimental; urgency=low

  * First official upload, closes: #399544.

 -- Romain Beauxis <toots@rastageeks.org>  Fri, 16 Mar 2007 14:26:50 +0100
